from typing import List


class TradingApp:
    def __init__(self, trading_strategy="average"):
        self.trading_strategy = trading_strategy

    def connect(self):
        print("Connecting to exchange")

    def get_market_data(self, coin: str) -> List[float]:
        return [800, 1000, 1800, 1490]

    def list_average(self, lis: List[float]) -> float:
        return sum(lis) / len(lis)

    def should_buy(self, prices: List[float]) -> bool:
        if self.trading_strategy == "minmax":
            return prices[-1] == min(prices)
        else:
            return prices[-1] < self.list_average(prices)

    def should_sell(self, prices: List[float]) -> bool:
        if self.trading_strategy == "minmax":
            return prices[-1] == max(prices)
        else:
            return prices[-1] > self.list_average(prices)

    def check_prices(self, coin: str):
        self.connect()
        prices = self.get_market_data(coin)
        should_buy = self.should_buy(prices)
        should_sell = self.should_sell(prices)
        if should_buy:
            print(f"You should buy {coin}.")
        elif should_sell:
            print(f"You should sell {coin}")
        else:
            print(f"Do nothing with coin: {coin}")


trading_app = TradingApp("average")
trading_app.check_prices("ETH/USDT")
