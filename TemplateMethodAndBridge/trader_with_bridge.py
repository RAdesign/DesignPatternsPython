""" bridge pattern with simple trading bot as an example, improved case"""
from abc import ABC, abstractmethod
from typing import List


class Exchange(ABC):
    @abstractmethod
    def connect(self):
        pass

    @abstractmethod
    def get_market_data(self, coin: str) -> List[float]:
        pass


class Bitfinex(Exchange):
    def connect(self):
        print("Connecting to Bitfinex...")

    def get_market_data(self, coin: str) -> List[float]:
        return [1100, 1450, 1600, 800, 1360, 1800]  # randomized prices for ETH/USDT pair


class KuCoin(Exchange):
    def connect(self):
        print("Connecting to KuCoin...")

    def get_market_data(self, coin: str) -> List[float]:
        return [1102, 1430, 1610, 800, 1350, 1807]  # randomized prices for ETH/USDT pair


class TraderBot(ABC):
    def __init__(self, exchange: Exchange):
        self.exchange = exchange

    def check_prices(self, coin: str):
        self.exchange.connect()
        prices = self.exchange.get_market_data(coin)
        should_buy = self.should_buy(prices)
        should_sell = self.should_sell(prices)
        if should_buy:
            print(f"You should buy {coin}.")
        elif should_sell:
            print(f"You should sell {coin}")
        else:
            print(f"Do nothing with coin: {coin}")

    @abstractmethod
    def should_buy(self, prices: List[float]) -> bool:
        pass

    @abstractmethod
    def should_sell(self, prices: List[float]) -> bool:
        pass


class AverageTraderBot(TraderBot):
    def average_prices(self, lst: List[float]) -> float:
        return sum(lst) / len(lst)

    def should_buy(self, prices: List[float]) -> bool:
        return prices[-1] < self.average_prices(prices)

    def should_sell(self, prices: List[float]) -> bool:
        return prices[-1] > self.average_prices(prices)


class MinMaxTraderBot(TraderBot):
    def should_buy(self, prices: List[float]) -> bool:
        return prices[-1] == min(prices)

    def should_sell(self, prices: List[float]) -> bool:
        return prices[-1] == max(prices)


trading_app = AverageTraderBot(Bitfinex())
trading_app.check_prices("ETH/USDT")
