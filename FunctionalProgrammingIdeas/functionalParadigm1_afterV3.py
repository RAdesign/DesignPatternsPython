# functional programming - version 3
from datetime import datetime
from functools import partial
from typing import Callable


GreetingReader = Callable[[], str]
GreetingFunction = Callable[[], str]


def greet(name: str, greeting_reader: GreetingReader) -> str:
    # special case , function on case "Rado" does not need to be called, as it has it`s return
    if name == "Rado":
        return "Get going"
    # end of special case, normal case below
    return f"{greeting_reader()}, {name}."


# extra functionality just for testing
def greet_list(names: list[str], greeting_fn: GreetingFunction) -> list[str]:
    return [greeting_fn(name) for name in names]


def read_greeting() -> str:
    current_time = datetime.now()
    if current_time.hour < 12:
        return "Good morning"
    elif 12 <= current_time.hour < 18:
        return "Good afternoon"
    else:
        return "Good evening"


def read_name() -> str:
    return input("Enter your name: ")


def main() -> None:
    # example of partial function application, splitting called argument (function)
    greet_fn = partial(greet, greeting_reader=read_greeting)
    print(greet_fn(read_name()))
    print(greet_list(["Jan", "Janina", "Janek"], greet_fn))


if __name__ == "__main__":
    main()
