""" immutable variables in functional paradigm or language:
easier with multi-threading, to understand, to test"""
import random

# a simple example


def main() -> None:
    test_list = [111, 67, -19, 0, 3, 64, 15, 99]

    # built in immutable sort example, keeps data, can be used on tuples, or any iterables as well
    sorted_list = sorted(test_list)
    print(f"Original list: {test_list}")
    print(f"Sorted list(immutable sort): {sorted_list}")

    # built in mutable sort (does a mess with data to be reused, can`t be used on tuples)
    test_list.sort()
    print(f"Original list(mutable sort) {test_list}")

    # cards sorting example, mutable vs. immutable example (try replacing with tuple
    cards = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"]

    shuffled_cards = random.sample(cards, k=len(cards))
    print(f"Shuffled cards: {shuffled_cards}")
    print(f"Pre-shuffled cards: {cards}")

    random.shuffle(cards)  # shuffles the cards (mutable mess when wanting to reuse, can`t be used on tuples)
    print(f"Cards(mutable sort): {cards}")


if __name__ == "__main__":
    main()
