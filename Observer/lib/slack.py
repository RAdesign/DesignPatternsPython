"""this is just a mockup method to fill in dependencies"""


def post_slack_message(channel: str, msg: str):
    print(f"[Slackbot - {channel}]: {msg}")
