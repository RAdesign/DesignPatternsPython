"""this is just a mockup method to fill in dependencies"""


def send_email(name: str, address: str, subject: str, body: str):
    print("-----------")
    print(f"Sending email to {name} ({address})")
    print("-----------")
    print(f"Subject: {subject}")
    print(body)
    print("Thanks, The Team")
    print("-----------")
