"""this is just a mockup method to fill in dependencies"""
from datetime import datetime


def logging(msg: str):
    print(f"{datetime.now()} - {msg}")
