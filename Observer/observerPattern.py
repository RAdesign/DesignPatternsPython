"""a pre pattern version of observer base"""

from api.user import register_new_user, password_forgotten
from api.plan import upgrade_plan

# register a new user
register_new_user("Rado", "123adminISpies", "my@email.pl")

# send a password reset message
password_forgotten("my@email.pl")

# upgrade the plan
upgrade_plan("my@email.pl")
