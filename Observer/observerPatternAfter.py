"""a post changes version of observer pattern"""
from apiV2.user import register_new_user, password_forgotten
from apiV2.plan import upgrade_plan
from apiV2.slack_listener import setup_slack_event_handlers
from apiV2.log_listener import setup_log_event_handlers
from apiV2.email_listener import setup_email_event_handlers

# prepare event structure
# it`s easy to disable each event just by commenting out
# any new event can be easily added as well
setup_email_event_handlers()
setup_slack_event_handlers()
setup_log_event_handlers()

# register a new user
register_new_user("Rado", "123adminISpies", "my@email.pl")

# send a password reset message
password_forgotten("my@email.pl")

# upgrade the plan
upgrade_plan("my@email.pl")
