""" upgrade - handling observer with events - a logging event handler """
from lib.log import logging
from .event import subscribe


def handle_user_registered_event(user):
    logging(f"User: '{user.name}' with email: '{user.email}', has been registered.")


def handle_user_password_forgotten_event(user):
    logging(f"User: '{user.name}' with email: '{user.email}', asked to reset their password.")


def handle_user_upgrade_plan_event(user):
    logging(f"User: '{user.name}' with email: '{user.email}', has upgraded their plan.")


def setup_log_event_handlers():
    subscribe("user_registered", handle_user_registered_event)
    subscribe("user_password_forgotten", handle_user_password_forgotten_event)
    subscribe("user_upgrade_plan", handle_user_upgrade_plan_event)
