from lib.email import send_email
from .event import subscribe


def handle_user_registered_event(user):  # sending confirmation email
    send_email(user.name, user.email, "Welcome!",
               f"Dear {user.name}, you have been successfully registered to our list.")


def handle_user_password_reset(user):  # sends password reset message
    send_email(user.name, user.email, "Resetting your password",
               f"To reset your password, use this code: {user.reset_code}.")


def handle_user_upgrade_plan(user):  # send conformation and thanks
    send_email(user.name, user.email, "Thank you for upgrading!",
               f"Dear {user.name}, you have successfully upgraded your plan.")


def setup_email_event_handlers():
    subscribe("user_registered", handle_user_registered_event)
    subscribe("user_password_forgotten", handle_user_password_reset)
    subscribe("user_upgrade_plan", handle_user_upgrade_plan)
