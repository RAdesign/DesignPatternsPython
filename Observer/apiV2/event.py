"""upgrade - handling observer with events"""
subscribers = dict()


def subscribe(event_type: str, fn):
    if event_type not in subscribers:  # checking if event is already in dict
        subscribers[event_type] = []  # if not, creates new
    subscribers[event_type].append(fn)  # if there is adds function/event to a dict


def post_event(event_type: str, data):
    if event_type not in subscribers:  # first check if data event type exists
        return
    for fn in subscribers[event_type]:  # cycle through all events in subscribers dict
        fn(data)  # run all event functions with specific data
