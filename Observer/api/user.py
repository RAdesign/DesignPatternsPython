from lib.email import send_email
from lib.db import create_user, find_user
from lib.log import logging
from lib.slack import post_slack_message  # pre event import
from lib.stringtools import get_random_string
from api.event import post_event


def register_new_user(name: str, password: str, email: str):
    # create database entry
    user = create_user(name, password, email)

    post_event("user registered", user)  # (event type, data) format used
    # # below a pre event way of posting events
    # # post a slack message to sales department
    # post_slack_message("sales", f"User {user.name} has registered with email {user.email}")
    #
    # # send welcome message
    # send_email(user.name, user.email, "Welcome",
    #            f"Dear {user.name}, thanks for registering with us!")
    #
    # # write server log
    # logging(f"User has registered with email address {user.email}.")


def password_forgotten(email: str):
    # retrieve the user
    user = find_user(email)

    # generate a password reset code
    user.reset_code = get_random_string(16)

    post_event("user_password_forgotten", user)

    # # below a pre event way of password reset info
    # # send a password reset message
    # send_email(user.name, user.email, "Resetting password "
    #            f"Dear {user.name}, to reset your password use this code: {user.reset_code}")
    #
    # # write server log
    # logging(f"User with email {user.email} has requested a password reset.")

