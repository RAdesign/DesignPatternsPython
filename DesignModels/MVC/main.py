"""MVC - Model-View-Controller - application design made by Steve Burbeck. View is a user GUI, Model is business logic applied with
backend programming, Controller: organizational part of GUI, that controls multiple screens and navigation
Sometimes View is just View, and Controller is considered a GUI with all underlying controls
However, the inter-coupling of M, V and C, is not always good. So there are also different models."""

from controller import Controller
from model import Model
from view import TodoList


def main() -> None:
    model = Model()
    view = TodoList(model)
    controller = Controller(model, view)
    controller.run()


if __name__ == "__main__":
    main()
