from PyQt6 import QtGui

TASK_LIST = [
    "Process emails",
    "Write post",
    "Prepare scripts",
    "Analyze data",
    "Swimming pool",
    "Dinner with gf",
]


def create_model() -> QtGui.QStandardItemModel:
    model = QtGui.QStandardItemModel()
    for task in TASK_LIST:
        item = QtGui.QStandardItem(task)
        model.appendRow(item)
    return model
