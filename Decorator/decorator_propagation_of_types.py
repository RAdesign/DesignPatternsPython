"""decorator example - a logger and benchmark wrapping counting function, but with a functools wrap,
expanding a problem - fiddling with type checking:
Typing system recognizes 'count_prime_numbers' as Callable[...,Any]
One can solve this by using other generics to help type checker, as:
-TypeVar
-ParamSpec (typing-extensions library)"""
import functools
import logging
from math import sqrt
from time import perf_counter
from typing import Any, Callable, TypeVar, ParamSpec


# creating generic types, TypeVar represents a single value
WrappedReturn = TypeVar("WrappedReturn")

# ParamSpec represents args and kwargs of a function signature
WrappedParams = ParamSpec("WrappedParams")


# changing decorator definitions to reflect generic types
def with_logging(func: Callable[WrappedParams, WrappedReturn]) -> Callable[WrappedParams, WrappedReturn]:
    @functools.wraps(func)
    # assigning generic types to args and kwargs
    def wrapper(*args: WrappedParams.args, **kwargs: WrappedParams.kwargs) -> WrappedReturn:
        logging.info(f"Calling function: {func.__name__}")
        value = func(*args, **kwargs)
        logging.info(f"Finished function: {func.__name__}")
        return value
    return wrapper


# changing decorator definitions to reflect generic types
def benchmark(func: Callable[WrappedParams, WrappedReturn]) -> Callable[WrappedParams,WrappedReturn]:
    @functools.wraps(func)
    # assigning generic types to args and kwargs
    def wrapper(*args: WrappedParams.args, **kwargs: WrappedParams.kwargs) -> WrappedReturn:
        start_timer = perf_counter()
        value = func(*args, **kwargs)
        end_timer = perf_counter()
        run_time_total = end_timer - start_timer
        logging.info(f"Execution of function: {func.__name__} took {run_time_total:.2f} seconds.\n"
                     f"There are {value} of prime numbers withing range.")
        return value
    return wrapper


def is_prime(number: int) -> bool:
    if number < 2:
        return False
    for item in range(2, int(sqrt(number)) + 1):
        if number % item == 0:
            return False
    return True


@with_logging
@benchmark
def count_prime_number(upper_bound: int) -> int:
    count = 0
    for number in range(upper_bound):
        if is_prime(number):
            count += 1
    return count


def main() -> None:
    logging.basicConfig(level=logging.INFO)
    count_prime_number(100000)  # type checker will show correct type : Callable[int, int]


if __name__ == "__main__":
    main()
