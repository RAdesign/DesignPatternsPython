""" 2. decorator example - a logger and benchmark wrapping a simple counting function.
this way is wrapping function in another function, to create a decorator"""
import logging
from math import sqrt
from time import perf_counter
from typing import Any, Callable


# logging is a decorator as a function
def with_logging(func: Callable[..., Any]) -> Callable[..., Any]:
    def wrapper(*args: Any, **kwargs: Any) -> Any:
        logging.info(f"Calling function: {func.__name__}")
        value = func(*args, **kwargs)
        logging.info(f"Finished function: {func.__name__}")
        return value
    return wrapper


# this time benchmark is not a decorator class, but a function
def benchmark(func: Callable[..., Any]) -> Callable[..., Any]:
    # to use benchmark on any function, a wrapper structure is created, and benchmark is made generic
    def wrapper(*args: Any, **kwargs: Any) -> Any:
        start_timer = perf_counter()
        value = func(*args, **kwargs)
        end_timer = perf_counter()
        run_time_total = end_timer - start_timer
        logging.info(f"Execution of function: {func.__name__} took {run_time_total:.2f} seconds.\n"
                     f"There are {value} of prime numbers withing range of {func}")
        return value
    return wrapper


def is_prime(number: int) -> bool:
    if number < 2:
        return False
    for item in range(2, int(sqrt(number)) + 1):
        if number % item == 0:
            return False
    return True


# @ notation works as a decorator
@with_logging
@benchmark
# this time counting is not a decorator class, but a function
def count_prime_number(upper_bound: int) -> int:
    count = 0
    for number in range(upper_bound):
        if is_prime(number):
            count += 1
    return count


def main() -> None:
    logging.basicConfig(level=logging.INFO)
    count_prime_number(100000)
    # instead of calling @ notation decorating 'count prime number' function, it could be also called:
    # wrapper_function = benchmark(count_prime_number)
    # value = wrapper_function(100000)
    # logging.info(f"Number of prime numbers is {value}")


if __name__ == "__main__":
    main()
