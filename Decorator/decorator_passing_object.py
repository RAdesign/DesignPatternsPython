""" 4. decorator example - a logger and benchmark wrapping counting function, with a functools wrap,
and a logger object and an and additional layer , but logger needs to be passed to decorator as argument"""
import functools
import logging
from math import sqrt
from time import perf_counter
from typing import Any, Callable

logger = logging.getLogger("decorator_app")


def with_logging(logger: logging.Logger):
    def decorator(func: Callable[..., Any]) -> Callable[..., Any]:
        @functools.wraps(func)
        def wrapper(*args: Any, **kwargs: Any) -> Any:
            logger.info(f"Calling function: {func.__name__}")
            value = func(*args, **kwargs)
            logger.info(f"Finished function: {func.__name__}")
            return value
        return wrapper
    return decorator


def benchmark(func: Callable[..., Any]) -> Callable[..., Any]:
    @functools.wraps(func)
    def wrapper(*args: Any, **kwargs: Any) -> Any:
        start_timer = perf_counter()
        value = func(*args, **kwargs)
        end_timer = perf_counter()
        run_time_total = end_timer - start_timer
        logger.info(f"Execution of function: {func.__name__} took {run_time_total:.2f} seconds.\n"
                    f"There are {value} of prime numbers withing range.")
        return value
    return wrapper


def is_prime(number: int) -> bool:
    if number < 2:
        return False
    for item in range(2, int(sqrt(number)) + 1):
        if number % item == 0:
            return False
    return True


@with_logging(logger)  # remember to pass a created logger object as an argument to decorator
@benchmark
def count_prime_number(upper_bound: int) -> int:
    count = 0
    for number in range(upper_bound):
        if is_prime(number):
            count += 1
    return count


def main() -> None:
    logging.basicConfig(level=logging.INFO)
    count_prime_number(100000)


if __name__ == "__main__":
    main()
