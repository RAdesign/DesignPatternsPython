""" 3. decorator example - a logger and benchmark wrapping counting function, but with a functools wrap,
it helps to get exact names of function called with logging decorator"""
import functools
import logging
from math import sqrt
from time import perf_counter
from typing import Any, Callable


# 'wraps' helps to get a proper name of called function in a logging mechanism
def with_logging(func: Callable[..., Any]) -> Callable[..., Any]:
    @functools.wraps(func)  # this helps to pass a name of a function properly
    def wrapper(*args: Any, **kwargs: Any) -> Any:
        logging.info(f"Calling function: {func.__name__}")
        value = func(*args, **kwargs)
        logging.info(f"Finished function: {func.__name__}")
        return value
    return wrapper


def benchmark(func: Callable[..., Any]) -> Callable[..., Any]:
    @functools.wraps(func)
    def wrapper(*args: Any, **kwargs: Any) -> Any:
        start_timer = perf_counter()
        value = func(*args, **kwargs)
        end_timer = perf_counter()
        run_time_total = end_timer - start_timer
        logging.info(f"Execution of function: {func.__name__} took {run_time_total:.2f} seconds.\n"
                     f"There are {value} of prime numbers withing range.")
        return value
    return wrapper


def is_prime(number: int) -> bool:
    if number < 2:
        return False
    for item in range(2, int(sqrt(number)) + 1):
        if number % item == 0:
            return False
    return True


@with_logging
@benchmark
def count_prime_number(upper_bound: int) -> int:
    count = 0
    for number in range(upper_bound):
        if is_prime(number):
            count += 1
    return count


def main() -> None:
    logging.basicConfig(level=logging.INFO)
    count_prime_number(100000)


if __name__ == "__main__":
    main()
