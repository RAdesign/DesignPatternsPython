"""6. decorator example - a logger and benchmark wrapping counting function, composition of abstract classes,
with a callable methods"""
import logging
from abc import ABC, abstractmethod
from math import sqrt
from time import perf_counter
from typing import Any, Callable


class AbstractDecorator(ABC):
    def __init__(self, decorated: Callable[..., Any]) -> None:
        self._decorated = decorated

    @abstractmethod
    def __call__(self, *args: Any, **kwargs: Any) -> Any:
        pass


class BenchmarkDecorator(AbstractDecorator):
    def __call__(self, *args: Any, **kwargs: Any) -> Any:
        start_timer = perf_counter()
        value = self._decorated(*args, **kwargs)
        end_timer = perf_counter()
        run_time_total = end_timer - start_timer
        logging.info(
            f"Execution of function: {self._decorated.__class__.__name__} took {run_time_total:.2f} seconds."
            f"There are {value} of prime numbers withing range."
        )
        return value


class LoggingDecorator(AbstractDecorator):
    def __call__(self, *args: Any, **kwargs: Any) -> Any:
        logging.info(f"Calling function: {self._decorated.__class__.__name__}")
        value = self._decorated(*args, **kwargs)
        logging.info(f"Finished function: {self._decorated.__class__.__name__}")
        return value


def is_prime(number: int) -> bool:
    if number < 2:
        return False
    for item in range(2, int(sqrt(number)) + 1):
        if number % item == 0:
            return False
    return True


def count_prime_number(upper_bound: int) -> int:
    count = 0
    for number in range(upper_bound):
        if is_prime(number):
            count += 1
    return count


def main() -> None:
    logging.basicConfig(level=logging.INFO)
    benchmark = BenchmarkDecorator(count_prime_number)
    with_logging = LoggingDecorator(benchmark)
    with_logging(100000)


if __name__ == "__main__":
    main()
