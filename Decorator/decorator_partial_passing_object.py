"""5. decorator example - using partial from functools to change need to pass a logger object to
a with_logging decorator
One could also move logger decorator to a different file"""
import functools
import logging
from math import sqrt
from time import perf_counter
from typing import Any, Callable

logger = logging.getLogger("decorator_app")


# 1 first version of with_logging, but the decorator acts as a function to be called ()
# def with_logging(logger: logging.Logger):
#     def decorator(func: Callable[..., Any]) -> Callable[..., Any]:
#         @functools.wraps(func)
#         def wrapper(*args: Any, **kwargs: Any) -> Any:
#             logger.info(f"Calling function: {func.__name__}")
#             value = func(*args, **kwargs)
#             logger.info(f"Finished function: {func.__name__}")
#             return value
#         return wrapper
#     return decorator

# 2 to use @with_default_logging as a proper decorator, change with_logging to this:
def with_logging(func: Callable[..., Any], logger: logging.Logger) -> Callable[..., Any]:
    @functools.wraps(func)
    def wrapper(*args: Any, **kwargs: Any) -> Any:
        logger.info(f"Calling function: {func.__name__}")
        value = func(*args, **kwargs)
        logger.info(f"Finished function: {func.__name__}")
        return value
    return wrapper


def benchmark(func: Callable[..., Any]) -> Callable[..., Any]:
    @functools.wraps(func)
    def wrapper(*args: Any, **kwargs: Any) -> Any:
        start_timer = perf_counter()
        value = func(*args, **kwargs)
        end_timer = perf_counter()
        run_time_total = end_timer - start_timer
        logger.info(f"Execution of function: {func.__name__} took {run_time_total:.2f} seconds.\n"
                    f"There are {value} of prime numbers withing range.")
        return value
    return wrapper


def is_prime(number: int) -> bool:
    if number < 2:
        return False
    for item in range(2, int(sqrt(number)) + 1):
        if number % item == 0:
            return False
    return True


with_default_logging = functools.partial(with_logging, logger=logger)


# @with_default_logging()  # 1 a decorator on a decorator, but needs to be called as a function, so not a decorator
@with_default_logging  # 2 proper decorator with changes to with_logging
@benchmark
def count_prime_number(upper_bound: int) -> int:
    count = 0
    for number in range(upper_bound):
        if is_prime(number):
            count += 1
    return count


def main() -> None:
    logging.basicConfig(level=logging.INFO)
    count_prime_number(100000)


if __name__ == "__main__":
    main()
