Decorator as a mean to implement external features with limited coupling.
(cross-cutting concerns)
In python a classic decorator pattern can be simplified
1. decorator as logging mechanism
2. decorator as benchmark
3. authentication, calling endpoints
4. user activities tracking

Problems:
1. decorators are not good with type checking
2. decorators can be unclear how they work, what is the sequence, etc
3. may modify functions signature