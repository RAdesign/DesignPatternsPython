""" 1. decorator example - a logger and benchmark wrapping counting function; classic, normal, classic
composition of abstract classes,"""
import logging
from abc import ABC, abstractmethod
from math import sqrt
from time import perf_counter


# basic component to be decorated - a simple function iterating through prime numbers in a range
def is_prime(number: int) -> bool:
    if number < 2:
        return False
    for item in range(2, int(sqrt(number)) + 1):
        if number % item == 0:
            return False
    return True


# abstract super class of all abstract elements, and adds parameter of number of iterations
class AbstractComponent(ABC):
    @abstractmethod
    def execute(self, upper_bound: int) -> int:
        pass


# abstract decorator that makes basic element a decorated element
class AbstractDecorator(AbstractComponent):
    def __init__(self, decorated: AbstractComponent) -> None:
        self._decorated = decorated

    @abstractmethod
    def execute(self, upper_bound: int) -> int:
        pass


# first decorator that counts prime numbers iterated in basic function
class ConcreteComponent(AbstractComponent):
    def execute(self, upper_bound: int) -> int:
        count = 0
        for number in range(upper_bound):
            if is_prime(number):
                count += 1
        return count


# a decorator that measures time of executing a basic function
class BenchmarkDecorator(AbstractDecorator):
    def execute(self, upper_bound: int) -> int:
        start_timer = perf_counter()
        value = self._decorated.execute(upper_bound)  # wraps around original 'execute' method
        end_timer = perf_counter()
        run_time_total = end_timer - start_timer
        logging.info(
            f"Execution of function: {self._decorated.__class__.__name__} took {run_time_total:.2f} seconds.\n"
            f"There are {value} of prime numbers withing range of {upper_bound}"
        )
        return value


class LoggingDecorator(AbstractDecorator):
    def execute(self, upper_bound: int) -> int:
        logging.info(f"Calling function: {self._decorated.__class__.__name__}")
        value = self._decorated.execute(upper_bound)
        logging.info(f"Finished function: {self._decorated.__class__.__name__}")
        return value


def main() -> None:
    logging.basicConfig(level=logging.INFO)
    component = ConcreteComponent()
    # another way of calling decorators
    # benchmark_decorator = BenchmarkDecorator(component)
    # logging_decorator = LoggingDecorator(benchmark_decorator)
    # logging_decorator.execute(100000)
    component = LoggingDecorator(component)
    component = BenchmarkDecorator(component)
    component.execute(100000)


if __name__ == "__main__":
    main()
