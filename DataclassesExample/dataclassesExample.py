"""dataclasses are data oriented classes, as normal classes are more method, function or event
oriented. It`s achieved by several mechanisms, like being able to represent object as a string,
comparing objects to other objects, easily defining data structure, and initialization method
cons :  if one forgets to discern class variable and instance variable"""

import random
import string
from dataclasses import dataclass, field


def generate_id() -> str:
    return "".join(random.choices(string.ascii_uppercase, k=12))


@dataclass(frozen=False, kw_only=True, match_args=True, slots=True)
# frozen makes instance read-only after initialization, no changes to values of existing dataclass objects
# kw_only makes initialization possible by providing keywords with value, like: name="Mike"
# match_args provides dunder methods to easy use of structural pattern matching
# slots is a faster way of accessing instance data fields than dunder __dict__
# but slots break when using multiple inheritance
class Person:
    name: str
    address: str
    active: bool = True
    email_address: list[str] = field(default_factory=list)
    # init false means default values can`t be overriden with instance initialization
    id: str = field(init=False, default_factory=generate_id)
    # search string is a way to make searching in database easier
    # underscores - _ for private , __ for protected, repr to exclude that field from being printed out
    _search_string: str = field(init=False, repr=False)

    # post init allows to add extra data parameters to dataclass object after instance initialization
    def __post_init__(self) -> None:
        self._search_string: str = f"{self.name}, {self.address}, {self.active}"

    # # version before dataclass
    # # print will return just object instance address
    # def __init__(self, name: str, address: str):
    #     self.name = name
    #     self.address = address
    # # print will return string representation of data
    # def __str__(self) -> str:
    #     return f"{self.name}, {self.address}"


def main() -> None:
    person = Person(name="Jan", address="2137 Yellow Str.")
    # dunder methods to access fields of instance like below, but use of slots is faster like 25%
    print(person.__dict__["name"])
    print(person)


if __name__ == "__main__":
    main()
