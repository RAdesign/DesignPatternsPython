import os

from mixpanel import Mixpanel


def post_event(event_type: str) -> None:
    m_token = os.getenv("MIXPANEL_KEY") or ""
    print(f"Sending event {event_type} to Mixpanel")
    m_panel = Mixpanel(m_token)
    m_panel.track("ID", event_type)


