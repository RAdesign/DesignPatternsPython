import json
from dataclasses import dataclass
from pathlib import Path

from gui import WorsePad


# this class is purely to manipulate configuration setting for testing
@dataclass
class Config:
    show_save_button: bool = True  # testing attribute with default value


def read_config_file() -> Config:
    config_file = Path.cwd() / "config.json"  # get specific config file from current working directory
    config_dict = json.loads(config_file.read_text())
    return Config(**config_dict)  # ** - pass unpacked values from variable/dictionary/config file


def main():
    config = read_config_file()
    app = WorsePad(config.show_save_button)
    app.mainloop()


if __name__ == "__main__":
    main()
