"""transaction abstract protocol class for example for command pattern"""
from typing import Protocol


class Transaction(Protocol):
    def execute(self) -> None:
        ...

    def undo(self) -> None:
        ...

    def redo(self) -> None:
        ...
