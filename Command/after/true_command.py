"""command main example for command pattern.
One can plan and execute commands in a batch, on certain time, structure"""
from bankBackend.bank import Bank
from bankBackend.commands import Batch, Deposit, Transfer, Withdrawal
from bankBackend.controller import BankController


def main() -> None:

    # bank instance creation
    new_bank = Bank()

    # bank controller instance creation
    new_bank_controller = BankController()

    # some accounts creation
    account_no1 = new_bank.create_account("Piglet")
    account_no2 = new_bank.create_account("Winnie")
    account_no3 = new_bank.create_account("Owl")

    # deposit sequence trying all methods
    new_bank_controller.execute(Deposit(account_no2, 1000_00))
    new_bank_controller.undo()
    new_bank_controller.redo()
    new_bank_controller.redo()

    # execute a batch of various commands - this shows flexibility of Command pattern, a system
    new_bank_controller.execute(
        Batch(   # aggregate commands for a batch
            commands=[
                Deposit(account_no3, 220_00),
                Deposit(account_no1, 220_00),
                # Withdrawal(account_no1, 99_000),  # an error to be raised - it will rollback an entire batch !
                Transfer(account_no2, account_no3, 110_00),
            ]
        )
    )

    # trying some undo-redo operations
    new_bank_controller.undo()
    new_bank_controller.undo()
    new_bank_controller.redo()
    new_bank_controller.redo()

    # try a withdrawal
    new_bank_controller.execute(Withdrawal(account_no3, 330_00))

    print(new_bank)


if __name__ == "__main__":
    main()
