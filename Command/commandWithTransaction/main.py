"""command main example for command pattern. With Transaction History variant -
what is more important of efficient - a current state, or transaction history.
Here, not balance, but list of transactions is the "true" state of application
In a state based it`s easier to view the current balance, in transaction based it`s easier to
modify sequence during txs like undo, redo.
Also , a different function to recompute balance in the background could be added, to get current balance"""
from bankBackend.bank import Bank
from bankBackend.command import Batch, Deposit, Transfer, Withdrawal
from bankBackend.controller import BankController


def main() -> None:

    # bank instance creation
    new_bank = Bank()

    # bank controller instance creation
    new_bank_controller = BankController()

    # some accounts creation
    account_no1 = new_bank.create_account("Piglet")
    account_no2 = new_bank.create_account("Winnie")
    account_no3 = new_bank.create_account("Owl")

    # deposit sequence trying all methods
    new_bank_controller.register_tx(Deposit(account_no2, 1000_00))
    new_bank_controller.undo()
    new_bank_controller.redo()
    new_bank_controller.redo()

    # execute a batch of various commands - this shows flexibility of Command pattern, a system
    new_bank_controller.register_tx(
        Batch(   # aggregate commands for a batch
            commands=[
                Deposit(account_no3, 220_00),
                Deposit(account_no1, 220_00),
                # Withdrawal(account_no1, 99_000),  # an error to be raised - it will rollback an entire batch !
                Transfer(account_no2, account_no3, 110_00),
            ]
        )
    )

    # try a withdrawal
    new_bank_controller.register_tx(Withdrawal(account_no3, 330_00))

    # clear cache, compute all balances
    new_bank.clear_cache()   # make sure everything is down to zero
    new_bank_controller.compute_balances()

    print(new_bank)


if __name__ == "__main__":
    main()
