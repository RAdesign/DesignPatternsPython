"""transaction abstract protocol class for example for command pattern, With Transaction variant"""
from typing import Protocol


class Transaction(Protocol):  # can be simplified due to redundance of extra undo/redo, as it works as
    # simply moving a pointer
    def execute(self) -> None:
        ...
