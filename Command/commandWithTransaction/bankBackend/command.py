"""command class for example for command pattern With Transaction variant"""
from dataclasses import dataclass, field

from bankBackend.account import Account  # keep full path
from bankBackend.transaction import Transaction


@dataclass
class Deposit:
    account: Account
    amount: int

    @property
    def transfer_detail(self) -> str:
        return f" {self.amount/100:.2f} units to account {self.account.name}."

    def execute(self) -> None:
        self.account.deposit(self.amount)
        print(f"Deposited {self.transfer_detail}")


@dataclass
class Withdrawal:
    account: Account
    amount: int

    @property
    def transfer_detail(self) -> str:
        return f" {self.amount / 100:.2f} units from account {self.account.name}."

    def execute(self) -> None:
        self.account.withdraw(self.amount)
        print(f"Withdrawn {self.transfer_detail}")


@dataclass
class Transfer:
    from_account: Account
    to_account: Account
    amount: int

    @property
    def transfer_detail(self) -> str:
        return f" {self.amount / 100:.2f} units from account {self.from_account.name} \n" \
               f" to account {self.to_account}."

    def execute(self) -> None:
        self.from_account.withdraw(self.amount)
        self.to_account.deposit(self.amount)
        print(f"Transferred {self.transfer_detail}")


@dataclass
class Batch:   # keeps track of commands in one batch, to be executed
    commands: list[Transaction] = field(default_factory=list)

    def execute(self) -> None:  # can be simplified now as txs are recorded
        for command in self.commands:
            command.execute()
