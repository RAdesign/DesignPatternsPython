"""controller class for example for command pattern With State variant"""
from dataclasses import dataclass, field

from bankBackend.transaction import Transaction  # keep full path


@dataclass
class BankController:
    ledger: list[Transaction] = field(default_factory=list)  # to register transactions
    current_tx: int = 0  # a pointer to a current position/tx in a ledger

    def register_tx(self, transaction: Transaction) -> None:  # transactions are not executed, but registered
        del self.ledger[self.current_tx:]  # first clear current index until the end of array/list
        # this up to prevent messing up tx history
        self.ledger.append(transaction)
        self.current_tx += 1  # pointer incremented

    def undo(self) -> None:  # because we have ledger, undo/redo can be simple now
        if self.current_tx > 0:
            self.current_tx -= 1

    def redo(self) -> None:
        if self.current_tx < len(self.ledger):
            self.current_tx += 1

    def compute_balances(self) -> None:   # actually executes balances
        for transaction in self.ledger[: self.current_tx]:  # execute until current index of tx
            transaction.execute()
