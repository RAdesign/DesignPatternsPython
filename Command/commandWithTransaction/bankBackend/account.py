"""account class for example for command pattern, in With Transaction variant"""
from dataclasses import dataclass


@dataclass
class Account:
    name: str
    number: str
    _balance_cache: int = 0  # private variable - as balance does not matter now, txs are counted

    def deposit(self, amount: int) -> None:
        self._balance_cache += amount

    def withdraw(self, amount: int) -> None:
        if amount > self._balance_cache:
            raise ValueError("Liquidity shortage")
        self._balance_cache -= amount

    def clear_cache(self) -> None:  # this method added for convenience
        # this is too general, as it should be changed to choose just certain accounts
        self._balance_cache = 0
