"""main example for command pattern, before the pattern, a banking 'app'
an IDE error in bank.py may occur, but without full path this will prompt 'module not found' error"""
from bankBackend.bank import Bank


def main() -> None:

    # create bank instance
    new_bank = Bank()

    # create accounts
    account_no1 = new_bank.create_account("Piglet")
    account_no2 = new_bank.create_account("Winnie")
    account_no3 = new_bank.create_account("Owl")

    account_no1.deposit(44_000)
    account_no2.deposit(55_000)
    account_no3.deposit(66_000)

    # make some transfers
    account_no1.withdraw(22_000)
    account_no3.deposit(22_000)
    account_no3.withdraw(33_000)

    print(new_bank)


if __name__ == "__main__":
    main()
