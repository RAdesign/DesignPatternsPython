"""controller class for example for command pattern With State variant"""
from dataclasses import dataclass, field

from bankBackend.transaction import Transaction


@dataclass
class BankController:
    undo_stack: list[Transaction] = field(default_factory=list)
    redo_stack: list[Transaction] = field(default_factory=list)

    def execute(self, transaction: Transaction) -> None:
        transaction.execute()
        self.redo_stack.clear()  # to clear any old transactions, this could be done in other place as well
        self.undo_stack.append(transaction)

    def undo(self) -> None:
        if not self.undo_stack:
            return
        transaction = self.undo_stack.pop()  # undone transaction is removed from stack
        transaction.undo()
        self.redo_stack.append(transaction)

    def redo(self) -> None:
        if not self.redo_stack:
            return
        transaction = self.redo_stack.pop()  # to have only actual transaction on the stack
        transaction.execute()
        self.undo_stack.append(transaction)
