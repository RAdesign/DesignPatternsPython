"""command class for example for command pattern With State variant"""
from dataclasses import dataclass, field

from bankBackend.account import Account
from bankBackend.transaction import Transaction


@dataclass
class Deposit:
    account: Account
    amount: int

    @property
    def transfer_detail(self) -> str:
        return f" {self.amount/100:.2f} units to account {self.account.name}."

    def execute(self) -> None:
        self.account.deposit(self.amount)
        print(f"Deposited {self.transfer_detail}")

    def undo(self) -> None:
        self.account.withdraw(self.amount)
        print(f"Cancellation of deposit of {self.transfer_detail}.")

    def redo(self) -> None:
        self.account.deposit(self.amount)
        print(f"Repeated deposit of {self.transfer_detail}.")


@dataclass
class Withdrawal:
    account: Account
    amount: int

    @property
    def transfer_detail(self) -> str:
        return f" {self.amount / 100:.2f} units from account {self.account.name}."

    def execute(self) -> None:
        self.account.withdraw(self.amount)
        print(f"Withdrawn {self.transfer_detail}")

    def undo(self) -> None:
        self.account.deposit(self.amount)
        print(f"Cancellation of withdrawal of {self.transfer_detail}.")

    def redo(self) -> None:
        self.account.withdraw(self.amount)
        print(f"Repeated withdrawal of {self.transfer_detail}.")


@dataclass
class Transfer:
    from_account: Account
    to_account: Account
    amount: int

    @property
    def transfer_detail(self) -> str:
        return f" {self.amount / 100:.2f} units from account {self.from_account.name} \n" \
               f" to account {self.to_account}."

    def execute(self) -> None:
        self.from_account.withdraw(self.amount)
        self.to_account.deposit(self.amount)
        print(f"Transferred {self.transfer_detail}")

    def undo(self) -> None:
        self.to_account.withdraw(self.amount)
        self.from_account.deposit(self.amount)
        print(f"Cancellation of transfer of {self.transfer_detail}.")

    def redo(self) -> None:
        self.from_account.withdraw(self.amount)
        self.to_account.deposit(self.amount)
        print(f"Repeated transfer of {self.transfer_detail}.")


@dataclass
class Batch:   # keeps track of commands in one batch, to be executed
    commands: list[Transaction] = field(default_factory=list)

    def execute(self) -> None:
        completed_commands: list[Transaction] = []  # to know what command is done
        try:
            for command in self.commands:
                command.execute()
                completed_commands.append(command)  # add executed commands to list
        except ValueError:
            for command in reversed(completed_commands):  # undo command fails
                command.undo()
            raise  # re-raise last exception, undo the undo

    def undo(self) -> None:
        for command in reversed(self.commands):
            command.undo()

    def redo(self) -> None:
        for command in self.commands:
            command.redo()
