# factory method as a video and audio exporting program

# cleaner version of factory method , version 2, subclasses, inheritance, abstract classes

from pathlib import Path
from abc import ABC, abstractmethod


class VideoExporter(ABC):
    # representation of video exporting codec
    @abstractmethod
    def prepare_export(self, video_data: str) -> None:
        """prepare video data for export"""

    @abstractmethod
    def do_export(self, folder: Path) -> None:
        """export video data to a folder"""


class LosslessVideoExporter(VideoExporter):
    # lossless video exporting codec
    def prepare_export(self, video_data: str) -> None:
        print("Preparing video data for lossless export.")

    def do_export(self, folder: Path) -> None:
        print(f"Exporting video data in lossless format to {folder}.")


class H264BVPVideoExporter(VideoExporter):
    """ H.264 video exporting codec with Baseline profile """

    def prepare_export(self, video_data: str) -> None:
        print("Preparing video data for H.264 (Baseline) export.")

    def do_export(self, folder: Path) -> None:
        print(f"Exporting video data in H.264 (Baseline) format to {folder}.")


class H264Hi422PVideoExporter(VideoExporter):
    """ H.264 video exporting codec with Hi422P profile (10-bit, 4:2:2 chroma sample."""

    def prepare_export(self, video_data: str) -> None:
        print("Preparing video data for H.264 (Hi422P) export.")

    def do_export(self, folder: Path) -> None:
        print(f"Exporting video data in H.264 (Hi422P) format to {folder}.")


class AudioExporter(ABC):
    """Basic representation of audio exporting codec"""
    @abstractmethod
    def prepare_export(self, audio_data: str) -> None:
        """prepare audio data for export"""

    @abstractmethod
    def do_export(self, folder: Path) -> None:
        """export audio data to a folder"""


class AACAudioExporter(AudioExporter):
    """AAC audio exporting codec"""

    def prepare_export(self, audio_data: str) -> None:
        print("Preparing audio data for AAC export")

    def do_export(self, folder: Path) -> None:
        print(f"Preparing audio data in AAC format to {folder}.")


class WAVAudioExporter(AudioExporter):
    """WAV (lossless) audio exporting codec"""

    def prepare_export(self, audio_data: str) -> None:
        print("Preparing audio data for WAV export")

    def do_export(self, folder: Path) -> None:
        print(f"Preparing audio data in WAV format to {folder}.")


class ExporterFactory(ABC):
    """A factory that represents a combination of a video and audio codecs
    This factory does not maintain any of those instances it creates"""

    def get_video_exporter(self) -> VideoExporter:
        """returns a new video exporter instance"""

    def get_audio_exporter(self) -> AudioExporter:
        """returns a new audio exporter instance"""


class FastExporter(ExporterFactory):
    """A factory to provide low quality but fast exporting"""

    def get_video_exporter(self) -> VideoExporter:
        return H264BVPVideoExporter()

    def get_audio_exporter(self) -> AudioExporter:
        return AACAudioExporter()


class HighQualityExporter(ExporterFactory):
    """A factory to provide high quality but slow exporting"""

    def get_video_exporter(self) -> VideoExporter:
        return H264Hi422PVideoExporter()

    def get_audio_exporter(self) -> AudioExporter:
        return AACAudioExporter()


class MasterQualityExporter(ExporterFactory):
    """A factory to provide the highest quality but slowest exporting"""

    def get_video_exporter(self) -> VideoExporter:
        return LosslessVideoExporter()

    def get_audio_exporter(self) -> AudioExporter:
        return WAVAudioExporter()


FACTORIES = {
        "low": FastExporter(),
        "high": HighQualityExporter(),
        "master": MasterQualityExporter()
    }


def read_factory() -> ExporterFactory:
    """Constructs an exporter factory based on user choices"""

    while True:
        export_quality = input(f"Enter desired quality ({', '.join(FACTORIES)}): "
                               )
        try:
            return FACTORIES[export_quality]
        except KeyError:
            print(f"Unknown quality option: {export_quality}.")


def do_export(fac: ExporterFactory) -> None:
    """Do a test export with specified video and audio exporter"""

    # create the video and audio exporters
    video_exporter = fac.get_video_exporter()
    audio_exporter = fac.get_audio_exporter()

    # prepare the export
    video_exporter.prepare_export("placeholder_for_video_data")
    audio_exporter.prepare_export("placeholder_for_audio_data")

    # do the export
    folder = Path("/user/tmp/video")
    video_exporter.do_export(folder)
    audio_exporter.do_export(folder)


def main():
    # create a factory
    factory = read_factory()

    # perform exporting
    do_export(factory)


if __name__ == "__main__":
    main()
