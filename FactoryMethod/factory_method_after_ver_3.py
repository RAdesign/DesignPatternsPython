# factory method as a video and audio exporting program

# factory method , version 3, using Protocol and Tuples instead of factory classes
# hint : use Protocol like an interface

from pathlib import Path
from typing import Protocol


class VideoExporter(Protocol):
    # representation of video exporting codec

    def prepare_export(self, video_data: str) -> None:
        """prepare video data for export"""

    def do_export(self, folder: Path) -> None:
        """export video data to a folder"""


class LosslessVideoExporter:
    # lossless video exporting codec
    def prepare_export(self, video_data: str) -> None:
        print("Preparing video data for lossless export.")

    def do_export(self, folder: Path) -> None:
        print(f"Exporting video data in lossless format to {folder}.")


class H264BVPVideoExporter:
    """ H.264 video exporting codec with Baseline profile """

    def prepare_export(self, video_data: str) -> None:
        print("Preparing video data for H.264 (Baseline) export.")

    def do_export(self, folder: Path) -> None:
        print(f"Exporting video data in H.264 (Baseline) format to {folder}.")


class H264Hi422PVideoExporter:
    """ H.264 video exporting codec with Hi422P profile (10-bit, 4:2:2 chroma sample."""

    def prepare_export(self, video_data: str) -> None:
        print("Preparing video data for H.264 (Hi422P) export.")

    def do_export(self, folder: Path) -> None:
        print(f"Exporting video data in H.264 (Hi422P) format to {folder}.")


class AudioExporter(Protocol):
    """Basic representation of audio exporting codec"""

    def prepare_export(self, audio_data: str) -> None:
        """prepare audio data for export"""

    def do_export(self, folder: Path) -> None:
        """export audio data to a folder"""


class AACAudioExporter:
    """AAC audio exporting codec"""

    def prepare_export(self, audio_data: str) -> None:
        print("Preparing audio data for AAC export")

    def do_export(self, folder: Path) -> None:
        print(f"Preparing audio data in AAC format to {folder}.")


class WAVAudioExporter:
    """WAV (lossless) audio exporting codec"""

    def prepare_export(self, audio_data: str) -> None:
        print("Preparing audio data for WAV export")

    def do_export(self, folder: Path) -> None:
        print(f"Preparing audio data in WAV format to {folder}.")


FACTORIES = {
        "low": (H264BVPVideoExporter, AACAudioExporter),
        "high": (H264Hi422PVideoExporter, AACAudioExporter),
        "master": (LosslessVideoExporter, WAVAudioExporter)
    }


def read_factory() -> tuple[VideoExporter, AudioExporter]:
    """Constructs an exporter factory based on user choices"""

    while True:
        export_quality = input(f"Enter desired quality ({', '.join(FACTORIES)}): "
                               )
        try:
            video_class, audio_class = FACTORIES[export_quality]
            return video_class(), audio_class()
        except KeyError:
            print(f"Unknown quality option: {export_quality}.")


def do_export(fac: tuple[VideoExporter, AudioExporter]) -> None:
    """Do a test export with specified video and audio exporter"""

    # create the video and audio exporters
    video_exporter, audio_exporter = fac

    # prepare the export
    video_exporter.prepare_export("placeholder_for_video_data")
    audio_exporter.prepare_export("placeholder_for_audio_data")

    # do the export
    folder = Path("/user/tmp/video")
    video_exporter.do_export(folder)
    audio_exporter.do_export(folder)


def main():
    # create a factory
    factory = read_factory()

    # perform exporting
    do_export(factory)


if __name__ == "__main__":
    main()
