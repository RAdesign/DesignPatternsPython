# factory method as a video and audio exporting program

# factory method , version 3, using Protocol and replacing Tuples with Dataclasses
# They return some flexibility, lost while applying Tuples, like Tuple needs certain order of use
# applying dunder methods to prepare factories

from pathlib import Path
from typing import Protocol, Type
from dataclasses import dataclass


class VideoExporter(Protocol):
    # representation of video exporting codec

    def prepare_export(self, video_data: str) -> None:
        """prepare video data for export"""

    def do_export(self, folder: Path) -> None:
        """export video data to a folder"""


class LosslessVideoExporter:
    # lossless video exporting codec
    def prepare_export(self, video_data: str) -> None:
        print("Preparing video data for lossless export.")

    def do_export(self, folder: Path) -> None:
        print(f"Exporting video data in lossless format to {folder}.")


class H264BVPVideoExporter:
    """ H.264 video exporting codec with Baseline profile """

    def prepare_export(self, video_data: str) -> None:
        print("Preparing video data for H.264 (Baseline) export.")

    def do_export(self, folder: Path) -> None:
        print(f"Exporting video data in H.264 (Baseline) format to {folder}.")


class H264Hi422PVideoExporter:
    """ H.264 video exporting codec with Hi422P profile (10-bit, 4:2:2 chroma sample."""

    def prepare_export(self, video_data: str) -> None:
        print("Preparing video data for H.264 (Hi422P) export.")

    def do_export(self, folder: Path) -> None:
        print(f"Exporting video data in H.264 (Hi422P) format to {folder}.")


class AudioExporter(Protocol):
    """Basic representation of audio exporting codec"""

    def prepare_export(self, audio_data: str) -> None:
        """prepare audio data for export"""

    def do_export(self, folder: Path) -> None:
        """export audio data to a folder"""


class AACAudioExporter:
    """AAC audio exporting codec"""

    def prepare_export(self, audio_data: str) -> None:
        print("Preparing audio data for AAC export")

    def do_export(self, folder: Path) -> None:
        print(f"Preparing audio data in AAC format to {folder}.")


class WAVAudioExporter:
    """WAV (lossless) audio exporting codec"""

    def prepare_export(self, audio_data: str) -> None:
        print("Preparing audio data for WAV export")

    def do_export(self, folder: Path) -> None:
        print(f"Preparing audio data in WAV format to {folder}.")


@dataclass
class MediaExporter:
    video: VideoExporter
    audio: AudioExporter


@dataclass
class MediaExporterFactory:
    video_class: Type[VideoExporter]
    audio_class: Type[AudioExporter]

    def __call__(self) -> MediaExporter:
        return MediaExporter(
            self.video_class(),
            self.audio_class()
        )


FACTORIES = {
        "low": MediaExporterFactory(H264BVPVideoExporter, AACAudioExporter),
        "high": MediaExporterFactory(H264Hi422PVideoExporter, AACAudioExporter),
        "master": MediaExporterFactory(LosslessVideoExporter, WAVAudioExporter),
    }


def read_factory() -> MediaExporterFactory:
    """Constructs an exporter factory based on user choices"""

    while True:
        export_quality = input(f"Enter desired quality ({', '.join(FACTORIES)}): "
                               )
        try:
            return FACTORIES[export_quality]
        except KeyError:
            print(f"Unknown quality option: {export_quality}.")


def do_export(exporter: MediaExporter) -> None:
    """Do a test export with specified video and audio exporter"""

    # prepare the export
    exporter.video.prepare_export("placeholder_for_video_data")
    exporter.audio.prepare_export("placeholder_for_audio_data")

    # do the export
    folder = Path("/user/tmp/video")
    exporter.video.do_export(folder)
    exporter.audio.do_export(folder)


def main():
    # create a factory
    factory = read_factory()

    # use factory to create the media exporter
    media_exporter = factory()

    # perform exporting
    do_export(media_exporter)


if __name__ == "__main__":
    main()
