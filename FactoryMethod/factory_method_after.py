# factory method as a video and audio exporting program

# video exporting example

import pathlib
from abc import ABC, abstractmethod


class VideoExporter(ABC):
    # representation of video exporting codec
    @abstractmethod
    def prepare_export(self, video_data):
        """prepare video data for export"""

    @abstractmethod
    def do_export(self, folder: pathlib.Path):
        """export video data to a folder"""


class LosslessVideoExporter(VideoExporter):
    # lossless video exporting codec
    def prepare_export(self, video_data):
        print("Preparing video data for lossless export.")

    def do_export(self, folder: pathlib.Path):
        print(f"Exporting video data in lossless format to {folder}.")


class H264BVPVideoExporter(VideoExporter):
    """ H.264 video exporting codec with Baseline profile """

    def prepare_export(self, video_data):
        print("Preparing video data for H.264 (Baseline) export.")

    def do_export(self, folder: pathlib.Path):
        print(f"Exporting video data in H.264 (Baseline) format to {folder}.")


class H264Hi422PVideoExporter(VideoExporter):
    """ H.264 video exporting codec with Hi422P profile (10-bit, 4:2:2 chroma sample."""

    def prepare_export(self, video_data):
        print("Preparing video data for H.264 (Hi422P) export.")

    def do_export(self, folder: pathlib.Path):
        print(f"Exporting video data in H.264 (Hi422P) format to {folder}.")


class AudioExporter(ABC):
    """Basic representation of audio exporting codec"""
    @abstractmethod
    def prepare_export(self, audio_data):
        """prepare audio data for export"""

    @abstractmethod
    def do_export(self, folder: pathlib.Path):
        """export audio data to a folder"""


class AACAudioExporter(AudioExporter):
    """AAC audio exporting codec"""

    def prepare_export(self, audio_data):
        print("Preparing audio data for AAC export")

    def do_export(self, folder: pathlib.Path):
        print(f"Preparing audio data in AAC format to {folder}.")


class WAVAudioExporter(AudioExporter):
    """WAV (lossless) audio exporting codec"""

    def prepare_export(self, audio_data):
        print("Preparing audio data for WAV export")

    def do_export(self, folder: pathlib.Path):
        print(f"Preparing audio data in WAV format to {folder}.")


class ExporterFactory(ABC):
    """A factory that represents a combination of a video and audio codecs
    This factory does not maintain any of those instances it creates"""

    def get_video_exporter(self) -> VideoExporter:
        """returns a new video exporter instance"""

    def get_audio_exporter(self) -> AudioExporter:
        """returns a new audio exporter instance"""


class FastExporter(ExporterFactory):
    """A factory to provide low quality but fast exporting"""

    def get_video_exporter(self) -> VideoExporter:
        return H264BVPVideoExporter()

    def get_audio_exporter(self) -> AudioExporter:
        return AACAudioExporter()


class HighQualityExporter(ExporterFactory):
    """A factory to provide high quality but slow exporting"""

    def get_video_exporter(self) -> VideoExporter:
        return H264Hi422PVideoExporter()

    def get_audio_exporter(self) -> AudioExporter:
        return AACAudioExporter()


class MasterQualityExporter(ExporterFactory):
    """A factory to provide the highest quality but slowest exporting"""

    def get_video_exporter(self) -> VideoExporter:
        return LosslessVideoExporter()

    def get_audio_exporter(self) -> AudioExporter:
        return WAVAudioExporter()


def read_exporter_type() -> ExporterFactory:
    """Constructs an exporter factory based on user choices"""

    factories = {
        "low": FastExporter(),
        "high": HighQualityExporter(),
        "master": MasterQualityExporter()
    }
    while True:
        export_quality = input("Enter desired quality (low, high, master): ")
        if export_quality in factories:
            return factories[export_quality]
        print(f"Unknown quality option: {export_quality}.")


def main(factory1: ExporterFactory) -> None:

    # normal way below, up in is dependency injection , passing factory1 as an argument
# def main() -> None:
    # factory1 = read_exporter_type()

    # create the video and audio exporters
    video_exporter = factory1.get_video_exporter()
    audio_exporter = factory1.get_audio_exporter()

    # prepare the export
    video_exporter.prepare_export("placeholder_for_video_data")
    audio_exporter.prepare_export("placeholder_for_audio_data")

    # do the export
    folder = pathlib.Path("/user/tmp/video")
    video_exporter.do_export(folder)
    audio_exporter.do_export(folder)


if __name__ == "__main__":
    factory1 = read_exporter_type()
    main(factory1)
    # main() # normal way, dependency injection 2 lines up above
