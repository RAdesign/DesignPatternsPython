"""Open-Closed SOLID principle in python, applied solution"""
from abc import ABC, abstractmethod


class Order:
    def __init__(self):
        self.items = []
        self.quantities = []
        self.prices = []
        self.status = "open"

    def add_item(self, name, quantity, price):
        self.items.append(name)
        self.quantities.append(quantity)
        self.prices.append(price)

    def total_price(self):
        total_sum = 0
        for item in range(len(self.prices)):
            total_sum += self.quantities[item] * self.prices[item]
        return total_sum


class PaymentProcessor(ABC):
    @abstractmethod
    def pay(self, this_order, security_code):
        pass


class DebitPaymentProcessor(PaymentProcessor):

    def pay(self, this_order, security_code):
        print("Processing debit payment.")
        print(f"Verifying security code: {security_code}.")
        this_order.status = "paid"


class CreditPaymentProcessor(PaymentProcessor):

    def pay(self, this_order, security_code):
        print("Processing credit payment.")
        print(f"Verifying security code: {security_code}.")
        this_order.status = "paid"


order_1 = Order()
order_1.add_item("Bracelet", 1, 99)
order_1.add_item("Ring", 2, 55)
order_1.add_item("Earrings", 4, 34)

print(order_1.total_price())
payment_processor = DebitPaymentProcessor()
payment_processor.pay(order_1, "123581321")
