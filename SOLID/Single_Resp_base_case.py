"""Single Responsibility SOLID principle in python shown as a base case to fix"""


class Order:  # Order class with far too many responsibilities
    def __init__(self):
        self.items = []
        self.quantities = []
        self.prices = []
        self.status = "open"

    def add_item(self, name, quantity, price):
        self.items.append(name)
        self.quantities.append(quantity)
        self.prices.append(price)

    def total_price(self):
        total_sum = 0
        for item in range(len(self.prices)):
            total_sum += self.quantities[item] * self.prices[item]
        return total_sum

    def pay(self, payment_type, security_code):
        if payment_type == "debit":
            print("Processing debit payment.")
            print(f"Verifying security code: {security_code}.")
            self.status = "paid"
        elif payment_type == "credit":
            print("Processing credit payment.")
            print(f"Verifying security code: {security_code}.")
            self.status = "paid"
        else:
            raise Exception(f"Unknown payment type: {payment_type}")


order = Order()
order.add_item("Bracelet", 1, 99)
order.add_item("Ring", 2, 55)
order.add_item("Earrings", 4, 34)

print(order.total_price())
order.pay("debit", "123581321")
