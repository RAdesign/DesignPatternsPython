"""Dependency Inversion SOLID principle in python, applied"""
from abc import ABC, abstractmethod


class Order:
    def __init__(self):
        self.items = []
        self.quantities = []
        self.prices = []
        self.status = "open"

    def add_item(self, name, quantity, price):
        self.items.append(name)
        self.quantities.append(quantity)
        self.prices.append(price)

    def total_price(self):
        total_sum = 0
        for item in range(len(self.prices)):
            total_sum += self.quantities[item] * self.prices[item]
        return total_sum


class Authorizer(ABC):
    @abstractmethod
    def is_authorized(self) -> bool:
        pass


class SMSAuthorizer(Authorizer):
    def __init__(self):
        self.authorized = False

    def sms_auth(self, sms_code):
        print(f"Verifying SMS code: {sms_code}.")
        self.authorized = True

    def is_authorized(self) -> bool:
        return self.authorized


class GoogleAuthAuthorizer(Authorizer):
    def __init__(self):
        self.authorized = False

    def sms_auth(self, sms_code):
        print(f"Verifying Google auth code: {sms_code}.")
        self.authorized = True

    def is_authorized(self) -> bool:
        return self.authorized


class HumanAuthorizer(Authorizer):
    def __init__(self):
        self.authorized = False

    def is_human(self):
        self.authorized = True

    def is_authorized(self) -> bool:
        return self.authorized


class PaymentProcessor(ABC):
    @abstractmethod
    def pay(self, this_order):
        pass


class DebitPaymentProcessor(PaymentProcessor):

    def __init__(self, security_code, authorizer: Authorizer):
        self.security_code = security_code
        self.authorizer = authorizer

    def pay(self, this_order):
        if not self.authorizer.is_authorized():
            raise Exception("Not authorized")
        print("Processing debit payment.")
        print(f"Verifying security code: {self.security_code}.")
        this_order.status = "paid"


class CreditPaymentProcessor(PaymentProcessor):

    def __init__(self, security_code):
        self.security_code = security_code

    def pay(self, this_order):
        print("Processing credit payment.")
        print(f"Verifying security code: {self.security_code}.")
        this_order.status = "paid"


class PayPalPaymentProcessor(PaymentProcessor):

    def __init__(self, email_address, authorizer: Authorizer):
        self.email_address = email_address
        self.authorizer = authorizer

    def pay(self, this_order):
        if not self.authorizer.is_authorized():
            raise Exception("Not authorized")
        print("Processing PayPal payment.")
        print(f"Verifying security email: {self.email_address}.")
        this_order.status = "paid"


order_1 = Order()
order_1.add_item("Bracelet", 1, 99)
order_1.add_item("Ring", 2, 55)
order_1.add_item("Earrings", 4, 34)

print(order_1.total_price())
authorizer = HumanAuthorizer()
authorizer.is_human()
payment_processor = PayPalPaymentProcessor("this_needs@email.now", authorizer)
payment_processor.pay(order_1)
