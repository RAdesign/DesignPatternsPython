"""Interface Segregation SOLID principle in python, applied"""
from abc import ABC, abstractmethod


class Order:
    def __init__(self):
        self.items = []
        self.quantities = []
        self.prices = []
        self.status = "open"

    def add_item(self, name, quantity, price):
        self.items.append(name)
        self.quantities.append(quantity)
        self.prices.append(price)

    def total_price(self):
        total_sum = 0
        for item in range(len(self.prices)):
            total_sum += self.quantities[item] * self.prices[item]
        return total_sum


class PaymentProcessor(ABC):
    @abstractmethod
    def pay(self, this_order):
        pass


class SMSPaymentProcessor(ABC):
    @abstractmethod
    def sms_auth(self, sms_code):
        pass

    @abstractmethod
    def pay(self, this_order):
        pass


class DebitPaymentProcessor(SMSPaymentProcessor):

    def __init__(self, security_code):
        self.security_code = security_code
        self.verified = False

    def sms_auth(self, sms_code):
        print(f"Verifying SMS code: {sms_code}.")
        self.verified = True

    def pay(self, this_order):
        if not self.verified:
            raise Exception("Transaction not authorized")
        print("Processing debit payment.")
        print(f"Verifying security code: {self.security_code}.")
        this_order.status = "paid"


class CreditPaymentProcessor(PaymentProcessor):

    def __init__(self, security_code):
        self.security_code = security_code

    def pay(self, this_order):
        print("Processing credit payment.")
        print(f"Verifying security code: {self.security_code}.")
        this_order.status = "paid"


class PayPalPaymentProcessor(SMSPaymentProcessor):

    def __init__(self, email_address):
        self.email_address = email_address
        self.verified = False

    def sms_auth(self, sms_code):
        print(f"Verifying SMS code: {sms_code}.")
        self.verified = True

    def pay(self, this_order):
        if not self.verified:
            raise Exception("Transaction not authorized")
        print("Processing PayPal payment.")
        print(f"Verifying security email: {self.email_address}.")
        this_order.status = "paid"


order_1 = Order()
order_1.add_item("Bracelet", 1, 99)
order_1.add_item("Ring", 2, 55)
order_1.add_item("Earrings", 4, 34)

print(order_1.total_price())
payment_processor = PayPalPaymentProcessor("this_needs@email.now")
payment_processor.sms_auth(9742)
payment_processor.pay(order_1)
