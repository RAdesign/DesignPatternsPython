import string
import random


def generate_id(length=8):
    return ''.join(random.choices(string.ascii_uppercase, k=length))


class SupportTicket:
    def __init__(self, customer, issue):
        self.id = generate_id()
        self.customer = customer
        self.issue = issue


class CustomerSupport:
    def __init__(self, processing_strategy: str = "fifo"):
        self.tickets = []
        self.processing_strategy = processing_strategy

    def create_ticket(self, customer, issue):
        self.tickets.append(SupportTicket(customer, issue))

    def process_tickets(self):
        # if empty do nothing
        if len(self.tickets) == 0:
            print("No tickets to process.")
            return
        if self.processing_strategy == "fifo":
            for ticket in reversed(self.tickets):
                self.process_ticket(ticket)
        elif self.processing_strategy == "random":
            list_copy = self.tickets.copy()
            random.shuffle(list_copy)
            for ticket in list_copy:
                self.process_ticket(ticket)

    def process_ticket(self, ticket: SupportTicket):
        print("_____________________")
        print(f"Processing ticket, id: {ticket.id}")
        print(f"Customer : {ticket.customer}")
        print(f"Issue : {ticket.issue}")
        print("_____________________")


# the main app
app = CustomerSupport("fifo")

# add few tickets
app.create_ticket("Fifi", "Computer dead")
app.create_ticket("Lulu", "Internet download slow")
app.create_ticket("Mimi", "Updates do not update")

# process tickets
app.process_tickets()
