import string
import random
from typing import List, Callable


def generate_id(length=8):
    return ''.join(random.choices(string.ascii_uppercase, k=length))


class SupportTicket:
    def __init__(self, customer, issue):
        self.id = generate_id()
        self.customer = customer
        self.issue = issue


def fifo_ordering(ticket_list: List[SupportTicket]) -> List[SupportTicket]:
    return ticket_list.copy()


def filo_ordering(ticket_list: List[SupportTicket]) -> List[SupportTicket]:
    list_copy = ticket_list.copy()
    list_copy.reverse()
    return list_copy


def random_ordering(ticket_list: List[SupportTicket]) -> List[SupportTicket]:
    list_copy = ticket_list.copy()
    random.shuffle(list_copy)
    return list_copy


def black_hole_ordering(ticket_list: List[SupportTicket]) -> List[SupportTicket]:
    return []


class CustomerSupport:
    def __init__(self):
        self.tickets = []

    def create_ticket(self, customer, issue):
        self.tickets.append(SupportTicket(customer, issue))

    def process_tickets(self, ordering: Callable[[List[SupportTicket]], List[SupportTicket]]):
        # create ordered list
        ticket_list = ordering(self.tickets)
        if len(self.tickets) == 0:
            print("No tickets to process.")
            return
        # go through tickets in a list with a strategy
        for ticket in ticket_list:
            self.process_ticket(ticket)

    def process_ticket(self, ticket: SupportTicket):
        print("_____________________")
        print(f"Processing ticket, id: {ticket.id}")
        print(f"Customer : {ticket.customer}")
        print(f"Issue : {ticket.issue}")
        print("_____________________")


app = CustomerSupport()

# add few tickets
app.create_ticket("Fifi", "Computer dead")
app.create_ticket("Lulu", "Internet download slow")
app.create_ticket("Mimi", "Updates do not update")

# process tickets with defined function
app.process_tickets(fifo_ordering)
