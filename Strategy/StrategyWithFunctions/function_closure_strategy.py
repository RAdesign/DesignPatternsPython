"""Closure strategy pattern based on a Trading Bot Example, with function instead of classes, using
closures allows for using a range of prices instead of passing a list - thus allowing to pass parameters
while calling a function as a parameter of bot class instance"""
import statistics
from dataclasses import dataclass
from typing import Callable
from exchange import Exchange


TradingStrategyFunction = Callable[[list[int]], bool]


# closure mechanism. closure function passes the parameter down, allowing for more control
def should_buy_avg_closure(window_size: int) -> TradingStrategyFunction:
    def should_buy_avg(prices: list[int]) -> bool:
        list_window = prices[-window_size:]
        return prices[-1] < statistics.mean(list_window)
    return should_buy_avg


def should_sell_avg_closure(window_size: int) -> TradingStrategyFunction:
    def should_sell_avg(prices: list[int]) -> bool:
        list_window = prices[-window_size:]
        return prices[-1] > statistics.mean(list_window)
    return should_sell_avg


# strategy based on minimum vs maximum
def should_buy_minmax_closure(max_price: int) -> TradingStrategyFunction:
    def should_buy_minmax(prices: list[int]) -> bool:
        return prices[-1] < max_price
    return should_buy_minmax


def should_sell_minmax_closure(min_price: int) -> TradingStrategyFunction:
    def should_sell_minmax(prices: list[int]) -> bool:
        return prices[-1] > min_price
    return should_sell_minmax


@dataclass
class TradingBot:
    # a bot that connects with exchange API and performs transactions
    exchange: Exchange
    buy_strategy: TradingStrategyFunction
    sell_strategy: TradingStrategyFunction

    def run(self, symbol: str) -> None:
        prices = self.exchange.get_market_data(symbol)

        if self.buy_strategy(prices):
            self.exchange.buy(symbol, 7)
        elif self.sell_strategy(prices):
            self.exchange.sell(symbol, 7)
        else:
            print(f"No trade done for {symbol}")


def main() -> None:
    # create an exchange and connection
    exchange = Exchange()
    exchange.connect()

    # create bot and run it once, with parameters of range passed down by closure function
    bot = TradingBot(exchange, should_buy_avg_closure(3), should_sell_minmax_closure(16_000_00))
    bot.run("BTC/USDT")


if __name__ == "__main__":
    main()
