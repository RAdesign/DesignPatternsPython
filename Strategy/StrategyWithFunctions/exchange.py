PRICE_DATA = {
    "BTC/USDT": [
        16_856_00,
        17_011_00,
        17_145_00,
        16_974_00,
        18_145_00,
        16_128_00,
        17_649_00,
        17_439_00,
        15_985_00,
        16_683_00,
    ],
    "ETH/USDT": [
        1_145_00,
        1_247_00,
        1_387_00,
        1_198_00,
        1_242_00,
        1_299_00,
        1_348_00,
        1_415_00,
        1_234_00,
        1_313_00,
    ],
}


class ExchangeConnectionError(Exception):
    """Custom error raised when there are troubles with connection"""


class Exchange:
    def __init__(self) -> None:
        self.connected = False

    def connect(self) -> None:
        # connect to the exchange
        print("Connecting to Exchange website...")
        self.connected = True

    def check_connection(self) -> None:
        # check if there is a connection
        if not self.connected:
            raise ExchangeConnectionError()

    def get_market_data(self, symbol: str) -> list[int]:
        # return market price "data" for given market symbol
        self.check_connection()
        return PRICE_DATA[symbol]

    def buy(self, symbol: str, amount: int) -> None:
        # emulate buying symbol for a given price
        self.check_connection()
        print(f"Bought {amount} of {symbol} for a market price.")

    def sell(self, symbol: str, amount: int) -> None:
        # emulate selling symbol for a given price
        self.check_connection()
        print(f"Sold {amount} of {symbol} for a market price.")
