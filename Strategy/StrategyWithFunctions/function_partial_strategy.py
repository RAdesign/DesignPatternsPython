"""strategy pattern based on a Trading Bot Example, with partial function instead of classes,
different from currying, partial can solve problems with passing parameters, and more"""
import statistics
from dataclasses import dataclass
from functools import partial
from typing import Callable
from exchange import Exchange


TradingStrategyFunction = Callable[[list[int]], bool]


# added window_size parameter to this function used as partial
def should_buy_avg(prices: list[int], window_size: int) -> bool:
    list_window = prices[-window_size:]
    return prices[-1] < statistics.mean(list_window)


def should_sell_avg(prices: list[int], window_size: int) -> bool:
    list_window = prices[-window_size:]
    return prices[-1] > statistics.mean(list_window)


# strategy based on minimum vs maximum, added max_price parameter to use function as a partial
def should_buy_minmax(prices: list[int], max_price: int) -> bool:
    return prices[-1] < max_price


def should_sell_minmax(prices: list[int], min_price: int) -> bool:
    return prices[-1] > min_price


@dataclass
class TradingBot:
    # a bot that connects with exchange API and performs transactions
    exchange: Exchange
    buy_strategy: TradingStrategyFunction
    sell_strategy: TradingStrategyFunction

    def run(self, symbol: str) -> None:
        prices = self.exchange.get_market_data(symbol)

        if self.buy_strategy(prices):
            self.exchange.buy(symbol, 7)
        elif self.sell_strategy(prices):
            self.exchange.sell(symbol, 7)
        else:
            print(f"No trade done for {symbol}")


def main() -> None:
    # create an exchange and connection
    exchange = Exchange()
    exchange.connect()

    # create partial applications of functions used by bot, and supply extra arguments, that we want
    buy_strategy = partial(should_buy_minmax, max_price=18_000_00)
    sell_strategy = partial(should_sell_avg, window_size=7)
    bot = TradingBot(exchange, buy_strategy, sell_strategy)
    bot.run("BTC/USDT")


if __name__ == "__main__":
    main()
