"""Strategy based on a Trading Bot Example, with function instead of classes,
because one does not have to store too many states"""
import statistics
from dataclasses import dataclass
from typing import Callable
from exchange import Exchange


TradingStrategyFunction = Callable[[list[int]], bool]


# strategy based on averages
def should_buy_avg(prices: list[int]) -> bool:
    list_window = prices[-3:]
    return prices[-1] < statistics.mean(list_window)


def should_sell_avg(prices: list[int]) -> bool:
    list_window = prices[-3:]
    return prices[-1] > statistics.mean(list_window)


# strategy based on minimum vs maximum
def should_buy_minmax(prices: list[int]) -> bool:
    # buy below 16k
    return prices[-1] < 16_000_00


def should_sell_minmax(prices: list[int]) -> bool:
    # sell above 18k
    return prices[-1] > 18_000_00


@dataclass
class TradingBot:
    # a bot that connects with exchange API and performs transactions
    exchange: Exchange
    buy_strategy: TradingStrategyFunction
    sell_strategy: TradingStrategyFunction

    def run(self, symbol: str) -> None:
        prices = self.exchange.get_market_data(symbol)

        if self.buy_strategy(prices):
            self.exchange.buy(symbol, 7)
        elif self.sell_strategy(prices):
            self.exchange.sell(symbol, 7)
        else:
            print(f"No trade done for {symbol}")


def main() -> None:
    # create an exchange and connection
    exchange = Exchange()
    exchange.connect()

    # create bot and run it once , with different strategies as parameters
    bot = TradingBot(exchange, should_buy_minmax, should_sell_avg)
    bot.run("BTC/USDT")


if __name__ == "__main__":
    main()
