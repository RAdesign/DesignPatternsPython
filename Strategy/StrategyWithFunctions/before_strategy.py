"""Strategy based on a Trading Bot Example"""
import statistics
from dataclasses import dataclass
from typing import Protocol

from exchange import Exchange


class TradingStrategy(Protocol):
    # trading strategy whether to buy or sell
    def should_buy(self, prices: list[int]) -> bool:
        raise NotImplementedError()

    def should_sell(self, prices: list[int]) -> bool:
        raise NotImplementedError()


class AverageTradingStrategy:
    # strategy based on averages
    def should_buy(self, prices: list[int]) -> bool:
        list_window = prices[-5:]
        print(f"Price mean {statistics.mean(list_window)}")
        return prices[-1] < statistics.mean(list_window)

    def should_sell(self, prices: list[int]) -> bool:
        list_window = prices[-5:]
        print(f"Price mean {statistics.mean(list_window)}")
        return prices[-1] > statistics.mean(list_window)


class MinMaxTradingStrategy:
    # strategy based on minimum vs maximum
    def should_buy(self, prices: list[int]) -> bool:
        # buy below 16k
        return prices[-1] < 16_000_00

    def should_sell(self, prices: list[int]) -> bool:
        # sell above 18k
        return prices[-1] > 18_000_00


@dataclass
class TradingBot:
    # a bot that connects with exchange API and performs transactions
    exchange: Exchange
    trading_strategy: TradingStrategy

    def run(self, symbol: str) -> None:
        prices = self.exchange.get_market_data(symbol)
        should_buy = self.trading_strategy.should_buy(prices)
        should_sell = self.trading_strategy.should_sell(prices)
        if should_buy:
            self.exchange.buy(symbol, 7)

        elif should_sell:
            self.exchange.sell(symbol, 7)
        else:
            print(f"No trade done for {symbol}")


def main() -> None:
    # create an exchange and connection
    exchange = Exchange()
    exchange.connect()

    # choose trading strategy
    # trading_strategy = MinMaxTradingStrategy()
    trading_strategy = AverageTradingStrategy()

    # create bot and run it once
    bot = TradingBot(exchange, trading_strategy)
    bot.run("BTC/USDT")


if __name__ == "__main__":
    main()
