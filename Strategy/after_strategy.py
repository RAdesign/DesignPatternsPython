import string
import random
from typing import List
from abc import ABC, abstractmethod


def generate_id(length=8):
    return ''.join(random.choices(string.ascii_uppercase, k=length))


class SupportTicket:
    def __init__(self, customer, issue):
        self.id = generate_id()
        self.customer = customer
        self.issue = issue


class TicketOrderingStrategy(ABC):
    @abstractmethod
    def create_ordering(self, ticket_list: List[SupportTicket]) -> List[SupportTicket]:
        pass


class FIFOOrderingStrategy(TicketOrderingStrategy):
    def create_ordering(self, ticket_list: List[SupportTicket]) -> List[SupportTicket]:
        return ticket_list.copy()


class FILOOrderingStrategy(TicketOrderingStrategy):
    def create_ordering(self, ticket_list: List[SupportTicket]) -> List[SupportTicket]:
        list_copy = ticket_list.copy()
        list_copy.reverse()
        return list_copy


class RandomOrderingStrategy(TicketOrderingStrategy):
    def create_ordering(self, ticket_list: List[SupportTicket]) -> List[SupportTicket]:
        list_copy = ticket_list.copy()
        random.shuffle(list_copy)
        return list_copy


class BlackHoleStrategy(TicketOrderingStrategy):
    def create_ordering(self, ticket_list: List[SupportTicket]) -> List[SupportTicket]:
        return []


class CustomerSupport:
    def __init__(self, processing_strategy: TicketOrderingStrategy):
        self.tickets = []
        self.processing_strategy = processing_strategy

    def create_ticket(self, customer, issue):
        self.tickets.append(SupportTicket(customer, issue))

    def process_tickets(self):
        # create ordered list
        ticket_list = self.processing_strategy.create_ordering(self.tickets)
        if len(self.tickets) == 0:
            print("No tickets to process.")
            return
        # go through tickets in a list with a strategy
        for ticket in ticket_list:
            self.process_ticket(ticket)

    def process_ticket(self, ticket: SupportTicket):
        print("_____________________")
        print(f"Processing ticket, id: {ticket.id}")
        print(f"Customer : {ticket.customer}")
        print(f"Issue : {ticket.issue}")
        print("_____________________")


app = CustomerSupport(RandomOrderingStrategy())

# add few tickets
app.create_ticket("Fifi", "Computer dead")
app.create_ticket("Lulu", "Internet download slow")
app.create_ticket("Mimi", "Updates do not update")

# process tickets
app.process_tickets()
