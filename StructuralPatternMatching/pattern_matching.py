# using complicated switches could be replaced best by patterns like Strategy
# however Pattern Matching is more than simple switch statement, an example
# applied in Python 3.10 and higher
# an example how to pattern-match command input keywords
# cons of matching: 1. sequence of cases is important/sensitive if similar values are used

from dataclasses import dataclass
from typing import List
import shlex


def run_command(command: str) -> None:
    match command:
        case "quit":
            print("Quitting the program")
            quit()
        case "erase":
            print("Erasing main drive now. NOT")
        case other:
            # !r to take care of printing double quotes and other such cases
            print(f"Command not known: {other!r}.")


def run_command_v2(command: str):
    # allows to use many parameters in command like : load file.py
    match command.split():
        case ["load", filename]:
            print(f"Loading file: {filename}.")
        case ["save", filename]:
            print(f"Saving to file: {filename}.")
        # *rest regards anything after the initial value choice
        case ["quit" | "exit" | "bye", *rest]:
            if "--force" in rest or "-f" in rest:
                print("Sends SIGTERM to all processes and quits the program")
                # use example : quit -f
            else:
                print("Quitting the program")
            quit()
        case _:
            print(f"Unknown command: {command!r}.")


def run_command_v3(command: str):
    match command.split():
        case ["load", filename]:
            print(f"Loading file: {filename}.")
        case ["save", filename]:
            print(f"Saving to file: {filename}.")
        # extended case conditions within case
        case ["quit" | "exit" | "bye", *rest] if "--force" in rest or "-f" in rest:
            print("Sends SIGTERM to all processes and quits the program")
            quit()
        case["quit" | "exit" | "bye"]:
            print("Quitting the program")
            quit()
        case _:
            print(f"Unknown command: {command!r}.")


@dataclass  # using object(class instance) instead of string as pattern match for command
class Command:

    command: str
    arguments: List[str]


def run_command_v4(command: Command):
    match command:
        case Command(command="load", arguments=[filename]):
            print(f"Loading file: {filename}.")
        case Command(command="save", arguments=[filename]):
            print(f"Saving to file: {filename}.")
        # extended case conditions within case
        case Command(command="quit" | "exit" | "bye", arguments=["--force" | "-f", *rest]):
            print("Sends SIGTERM to all processes and quits the program")
            quit()
        case Command(command="quit" | "exit" | "bye"):
            print("Quitting the program")
            quit()
        case _:
            print(f"Unknown command: {command!r}.")


def main() -> None:
    """Main function"""

    while True:
        # command = input("$ ") # base for ver 1,2
        # print(command) # old version 1
        # run_command_v2(command)  # running match command 2
        # dataclass version:
        # command, *arguments = input("$ ").split() # without shlex library
        command, *arguments = shlex.split(input("$ "))
        run_command_v4(Command(command, arguments))


if __name__ == "__main__":
    main()
