# Model View Controller example base case to refactor, a unique user id generator

import tkinter as tk
import uuid


class UUIDGen:

    def __init__(self):
        # setup tkinter
        self.root = tk.Tk()
        self.root.geometry("600x600")
        self.root.title("UUIDGen")

        # create gui
        self.frame = tk.Frame(self.root)
        self.frame.pack(fill=tk.BOTH, expand=1)
        self.label = tk.Label(self.frame, text="Result:")
        self.label.pack()
        self.list = tk.Listbox(self.frame)
        self.list.pack(fill=tk.BOTH, expand=1)
        self.generate_uuid_button = tk.Button(self.frame, text="Generate UUID", command=self.handle_click_gen_uuid)
        self.generate_uuid_button.pack()
        self.clear_button = tk.Button(self.frame, text="Clear list", command=self.handle_click_clear_list)
        self.clear_button.pack()

        # initialize uuid list
        self.uuid = []

        # initialize mainloop
        self.root.mainloop()

    def handle_click_gen_uuid(self):
        # generate uuid , add it to the list
        self.uuid.append(uuid.uuid4())
        self.list.insert(tk.END, self.uuid[-1])

    def handle_click_clear_list(self):
        # clear uuid list and remove it from the list
        self.uuid = []
        self.list.delete(0, tk.END)


# run the app
u_gen = UUIDGen()
