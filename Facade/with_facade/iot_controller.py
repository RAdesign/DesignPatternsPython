"""this could be integrated into gui, but is left to add other types of logging, mailing, etc.
controller separates behaviour(logging) and controlling the IOT device from GUI"""
import logging
from typing import Protocol


# now controller relies on facade as an interface, with its functions
class IOTFacade(Protocol):
    def power_speaker(self, on: bool) -> None:
        ...

    def get_status(self) -> str:
        ...


def get_status(iot: IOTFacade) -> str:
    logging.info(f"Display status for IOT devices.")
    status = iot.get_status()
    logging.info(f"Status: {status}")
    return status


def power_speaker(on: bool, iot: IOTFacade) -> None:
    logging.info(f"Turning speaker: {on}.")
    iot.power_speaker(on)
    logging.info("Message sent to speaker.")
