"""devices, managing creating , adding id, using protocol for partial inheritance(of attributes),
while not being a subclass"""
import random
import string
from typing import Protocol


# simple id generation
def generate_id(length: int = 8):
    return "".join(random.choices(string.ascii_uppercase, k=length))


class Device(Protocol):
    def connect(self) -> None:
        ...

    def disconnect(self) -> None:
        ...

    def connection_info(self) -> tuple[str, int]:
        ...

    def status_update(self) -> str:
        ...


class IOTService:
    def __init__(self):
        # dictionary list of devices with generated id as keys
        self._devices: dict[str, Device] = {}

    # register device by generating id and adding key:value pair to dictionary
    def register_device(self, device: Device) -> str:
        device.connect()
        device_id = generate_id()
        self._devices[device_id] = device
        return device_id

    # unregister device by finding and deleting key:value pair from dict
    def unregister_device(self, device_id: str) -> None:
        self._devices[device_id].disconnect()
        del self._devices[device_id]

    # return device by its key
    def get_device(self, device_id: str) -> Device:
        return self._devices[device_id]

    # return the entire devices dictionary
    def devices(self) -> dict[str, Device]:
        return self._devices
    