"""class defining abstract connection with methods"""


class Connection:
    def __init__(self, host: str, port: int):
        self.host = host
        self.port = port

    def connect(self):
        print(f"Connecting to host: {self.host} with port: {self.port}")

    def disconnect(self):
        print(f"Disconnecting from host: {self.host} and port: {self.port}")

    def send(self, data: str):
        print(f"Sending data to host: {self.host} with port: {self.port}")
        print(f"Data : {data}")
