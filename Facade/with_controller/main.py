"""main now works to connect everything according to MVC model, but main function still needs to know
about service , IOT, etc. controller still to close for a layer"""
import logging
from functools import partial

from gui import SmartApp
from iot.service import IOTService
from iot.devices import SmartSpeakerDevice
from iot_controller import get_status, power_speaker


def main():
    logging.basicConfig(level=logging.INFO)

    # create an IOT service separated into main now
    service = IOTService()

    # create smart speaker with unique id separated into main now
    smart_speaker = SmartSpeakerDevice()
    speaker_id = service.register_device(smart_speaker)
    # using partial to create a new version of power_speaker function, and supply it with new arguments
    power_speaker_fn = partial(power_speaker, service=service, speaker_id=speaker_id)
    get_status_fn = partial(get_status, service=service)

    # after connecting GUI with controller above, use new functions instances in new app instance
    app = SmartApp(power_speaker_fn, get_status_fn)
    app.mainloop()


if __name__ == "__main__":
    main()
